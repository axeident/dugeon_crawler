# README #

### Dugeon Crawler ###
*Thank you ,Sir, on your spaciousness chest and your help.*
-HudaAtef, January 2015

### How do I get set up? ###

* To use sourcetree:
Set it up so that the repoistory is saved to a file on the computer using the 'clone' option in sourcetree.

* After editing files, select them and 'stage' them. Then, once the edits you want to be offical have been made, write a summary in the commit box and commit them. Then, 'push' when you want them to be on the offical version that other people can access. Else it will be a locally saved new version, but not avalible to others.

* If there is a number next to the 'pull' option, then there are changes to the offical veriosn, and you will want to pull those first. This will require you to commit or discard all the changes made that had not been committed.

* You can start a 'branch,' which allows you to push a version which does not edit the main version of the files, and can later be merged back in.

### Contribution guidelines ###

Try to make sure everything is very well commented, as per normal ocidng standard.
There is a seperate folder in the folder's root for the maya files and for the apks
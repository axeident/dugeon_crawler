﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class SpawnScript : MonoBehaviour {
    
    public GameObject[] allMonsterPrefabs;      // Array holding all of the possible monsters that can be spawned
    private List<GameObject> mMonsters;         // hold all Monsters which have spawned

    public int mMaxMonstersOnscreen = 1;     // Qtd of Monsters to be Spawned
    public float mTimeToSpawn = 1f;     // Time to spawn the Monsters

    private bool mPositionSet;          // define if position was set

    private float minSpawnDistance = 10f;       // Closest a monster can be to player on spawn
    private float maxSpawnDistance = 20f;       // furthest from player a monster will spawn

    /// <summary>
    /// Repeated loop which spawns monsters until the max number of monsters has been reached
    /// ***want to change to max number of monsters onscreen rather than number spawned. 
    /// </summary>
    /// <returns></returns>
    private IEnumerator SpawnLoop()
    {
        // Defining the Spawning Position
        StartCoroutine(ChangePosition());
        while (true) {
            yield return new WaitForSeconds(0.2f);
            //remove any deleted spawns from the list
            mMonsters.RemoveAll(p => p == null);
            if (mMonsters.Count < mMaxMonstersOnscreen)
            {
                mMonsters.Add(SpawnElement());
                yield return new WaitForSeconds(Random.Range(mTimeToSpawn, mTimeToSpawn * 3));
            }
        }
    }

    /// <summary>
    /// Spawns a monster
    /// </summary>
    /// <returns>The spawned Monster gameObject</returns>
    private GameObject SpawnElement()
    {
        // spawn the element on a random position, inside a imaginary sphere
        //GameObject Monster = Instantiate(allMonsterPrefabs[Random.Range(0,allMonsterPrefabs.Length)], (Random.insideUnitSphere * 4) + transform.position, transform.rotation) as GameObject;
		Vector3 SpawnPosition = new Vector3(Random.Range(-20,20), 0, 2*maxSpawnDistance);
		GameObject Monster = Instantiate(allMonsterPrefabs[Random.Range(0,allMonsterPrefabs.Length)], SpawnPosition, transform.rotation) as GameObject;


        // define a random scale for the Monster
        float scale = Random.Range(0.5f, 2f);
        // change the Monster scale
        Monster.transform.localScale = new Vector3(scale, scale, scale);
        return Monster;
    }

    /// <summary>
    /// Determies the transfrom position which controls where the monsters spawn
    /// </summary>
    /// <returns></returns>
    private bool SetPosition()
    {
        // get the camera position
        Transform cam = Camera.main.transform;
        transform.position = cam.position;
        transform.Rotate(new Vector3(0, Random.Range(0f, 360f), 0));
        transform.Translate(new Vector3(Random.Range(minSpawnDistance, maxSpawnDistance), 0 , Random.Range(minSpawnDistance, maxSpawnDistance)));
        // set the position 10 units forward from the camera position
        //transform.position = cam.forward * 10;
        return true;
    }

    void Start()
    {

        // Initialize Monsters array according to
        // the desired quantity
        //mMonsters = new GameObject[mTotalMonsters];
        mMonsters = new List<GameObject>();

        // Initializing spawning loop
        StartCoroutine(SpawnLoop());
    }

    /// <summary>
    /// Coroutine used to wait for a small amount of time before setting the position
    /// </summary>
    /// <returns></returns>
    private IEnumerator ChangePosition()
    {

        yield return new WaitForSeconds(0.2f);
        // Define the Spawn position only once
        if (!mPositionSet)
        {
            // change the position only if Vuforia is active
            if (VuforiaBehaviour.Instance.enabled)
                SetPosition();
        }
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update () {
        SetPosition();
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SigilScript : MonoBehaviour {
    /////////////////////////////////////////
    //
    //             Camera Vars
    //
    /////////////////////////////////////////

    public float mScaleMax = 2f;        // Items's Max/Min scale
    public float mScaleMin = 0.5f;
    private Vector3 mItemMaxScale;   // Max Item Scale
    public float mGrowingSpeed = 10f;   // Growing Speed
    private bool mIsItemScaled = false;

    private Vector3 mOrbitDirection;    // Orbit direction

    /////////////////////////////////////////
    //
    //             Game Vars
    //
    /////////////////////////////////////////
    public int SigilID = 0;
    private GameObject textBox;
    public Material[] sigilMaterials;


	// Use this for initialization
	void Start () {
        // defining scale
        float scale = Random.Range(mScaleMin, mScaleMax);
        //mItemMaxScale = new Vector3(scale, scale, scale);
        mItemMaxScale = new Vector3(1.5f, 1.5f, 1.5f);

        // set Monster scale to 0, to grow it lates 
        transform.localScale = Vector3.zero;
        textBox = GameObject.Find("Sigil_" + SigilID.ToString());

        transform.Rotate(new Vector3(0, 90, 0));
        mOrbitDirection = new Vector3(0, Random.Range(-1f,1f), 0);
    }
	
	// Update is called once per frame
	void Update () {
        // scale Items if needed
        if (!mIsItemScaled)
            ScaleObj();
        // rotating around its axis
        transform.Rotate(mOrbitDirection * 30 * Time.deltaTime);

		// If coin is clicked/tapped pick it up.
		if (Input.GetMouseButtonDown(0)){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			// pick up clicked coin
			if (Physics.Raycast(ray, out hit)){
				if (hit.collider.gameObject == this.gameObject) {
					CollectSigil();
				}
			}
		}
    }

    /// <summary>
    /// Scales the object from size 0% to 100%
    /// </summary>
    private void ScaleObj()
    {

        // growing obj
        if (transform.localScale != mItemMaxScale)
            transform.localScale = Vector3.Lerp(transform.localScale, mItemMaxScale, Time.deltaTime * mGrowingSpeed);
        else
            mIsItemScaled = true;
    }

    /// <summary>
    /// Called after initilization to make it so that object collection increments the correct counter.
    /// Sets the dropped item to the correct material
    /// </summary>
    /// <param name="in_ID">integer value representing the sigil ID number that the item drop will be set to</param>
    public void setSigilID(int in_ID)
    {
        SigilID = in_ID;
        Debug.Log(SigilID);
        if (SigilID <= sigilMaterials.Length)
        {
            gameObject.GetComponent<Renderer>().material = sigilMaterials[SigilID - 1];
        }
        else
        {
            Debug.Log("Dropped sigil ID out of bounds!");
        }
    }

    /// <summary>
    /// Called via unity event trigger
    /// </summary>
    public void CollectSigil()
    {
        if (textBox != null)
        {
            textBox.GetComponent<HUDSigilBehaviorScript>().increaseValue();
            Destroy(gameObject);
        }
        else
        {
            Debug.Log("No Inventory Item Set");
            Destroy(gameObject);
        }
    }

}

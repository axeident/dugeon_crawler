﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class PortalControl : MonoBehaviour {

	public string SceneName;

	public void OnTriggerEnter(Collider other) {
		if (other.gameObject.CompareTag("MainCamera")){
			SceneManager.LoadScene (SceneName);
		}
		Debug.Log ("Portal Triggered.");
	}
}

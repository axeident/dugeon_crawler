﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviorScript : MonoBehaviour {
    public int mPlayerHealth = 1000;
    public Slider playerHealthSlider;
    public bool mIsAlive = true;
	public int monsterDeaths = 0;
	public int monsterTarget = 5;
	public Text KillCounter;
	public float speed = 5.0f;
	public float rotationspeed = 100.0f;
	public float dragSpeed = 25.0f;

	private GameObject Sigil1;
	private GameObject Sigil2;
	private GameObject Sigil3;
	private GameObject Sigil4;
	private GameObject Gate;
	private GameObject[] MonsterList;

    // Use this for initialization
    void Start () {
		playerHealthSlider = FindObjectOfType<Slider> ();
		Sigil1 = GameObject.Find("Sigil_1");
		Sigil2 = GameObject.Find("Sigil_2");
		Sigil3 = GameObject.Find("Sigil_3");
		Sigil4 = GameObject.Find("Sigil_4");
		Gate = GameObject.Find ("DreadPortalOfDoom");
		Gate.SetActive (true);
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (mIsAlive == false)
        {
            Death();
        }

		// Check for portal
		MonsterList = GameObject.FindGameObjectsWithTag("Monster");
		if (MonsterList.Length == 0){
			Gate.SetActive (true);
		}
			

        if (playerHealthSlider != null) { 
        	// Updates the healthSlider each frame
        	playerHealthSlider.value = mPlayerHealth;
    	}
		KillCounter.text = "Monster Kills: " + monsterDeaths.ToString ();


		//Player Movement Keyboard
		float translation = Input.GetAxis ("Vertical") * speed;
		float rotation = Input.GetAxis ("Horizontal") * rotationspeed;
		translation *= Time.deltaTime;
		rotation *= Time.deltaTime;
		this.gameObject.transform.Translate (0, 0, translation);
		this.gameObject.transform.Rotate (0, rotation, 0);


		//PC camera controls
		if (Input.GetMouseButton(1)) {
			float rotationY = Input.GetAxis ("Mouse Y") * dragSpeed;
			rotationY = Mathf.Clamp (rotationY, -60, 60);
			transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
		}

	}

    /// <summary>
    /// Called when the player is hit and takes damage
    /// Sets isAlive to false in the event of playerdeath
    /// </summary>
    /// <param name="hitDamage"> amount of damage recieved by the player</param>
    public void HitPlayer(int hitDamage)
    {
        Debug.Log("Player hit for " + hitDamage);
        mPlayerHealth -= hitDamage;
        if (mPlayerHealth <= 0 && mIsAlive)
        {
            this.mIsAlive = false;
        }
    }

    /// <summary>
    /// Handles player death event
    /// </summary>
    private void Death()
    {
        Debug.Log("Player Died");
		// Stop everything, display Game Over Screen.
    }

	public void DamageAbility (){
		if (Sigil1.GetComponent<HUDSigilBehaviorScript>().isComplete)  {
			Sigil1.GetComponent<HUDSigilBehaviorScript> ().decreaseValue ();
			GameObject[] MonsterList = GameObject.FindGameObjectsWithTag ("Monster");
			foreach (GameObject monster in MonsterList) {
				monster.GetComponent<MonsterBehaviorScript> ().Hit (100);
			}
		}
	}

	public void SlowAbility(){
		if (Sigil2.GetComponent<HUDSigilBehaviorScript>().isComplete)  {
			Sigil2.GetComponent<HUDSigilBehaviorScript> ().decreaseValue ();
			GameObject[] MonsterList = GameObject.FindGameObjectsWithTag ("Monster");
			foreach (GameObject monster in MonsterList) {
				monster.GetComponent<MonsterBehaviorScript> ().MoveSpeed -= monster.GetComponent<MonsterBehaviorScript> ().MoveSpeed/2;
				// monster.GetComponent<MonsterBehaviorScript> ().MoveSpeed += monster.GetComponent<MonsterBehaviorScript> ().MoveSpeed;
			}
		}
	}

	public void ItemGrabAbility(){
		if (Sigil3.GetComponent<HUDSigilBehaviorScript>().isComplete)  {
			Sigil3.GetComponent<HUDSigilBehaviorScript> ().decreaseValue ();
			GameObject[] ItemList = GameObject.FindGameObjectsWithTag ("Loot");
			foreach (GameObject item in ItemList) {
				item.GetComponent<SigilScript> ().CollectSigil (); 
			}
		}
	}

}

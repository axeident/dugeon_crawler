﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDSigilBehaviorScript : MonoBehaviour {
    private Text counterText;
    private Image sigilIcon;
    int collected = 0;
    public int RequiredForPortal = 10;
    public Color completeColor;
    public bool isComplete = false;


    // Use this for initialization
    void Start () {
        counterText = GetComponent<Text>();
        sigilIcon = GetComponentInChildren<Image>();
    }
	
	// Update is called once per frame
	void Update () {
	}

    /// <summary>
    /// Called from the sigil script to increase the counter of the relevant sigil
    /// </summary>
    public void increaseValue()
    {
        //if (isComplete ==false)
        //{
            collected++;
            counterText.text = collected.ToString();
        //}
        if (collected >= RequiredForPortal)     // Called when the number of sigils required to activate the associated ability have been claimed
        {
            isComplete = true;
        //    sigilIcon.color = completeColor;
        //    counterText.text = "Done!";
		}
    }

	public void decreaseValue()
	{
		collected -= RequiredForPortal;
		counterText.text = collected.ToString ();
		if (collected < RequiredForPortal)
		{
			isComplete = false;
		}
	}
}

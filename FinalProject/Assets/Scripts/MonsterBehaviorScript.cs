﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.AI;


public class MonsterBehaviorScript : MonoBehaviour
{
    /////////////////////////////////////////
    //
    //             Camera Vars
    //
    /////////////////////////////////////////

    public float mScaleMax = 2f;        // Monster's Max/Min scale
    public float mScaleMin = 0.5f;
    public float mOrbitMaxSpeed = 10f;  // Orbit max Speed
    private float mOrbitSpeed;          // Orbit speed
    private Transform mOrbitAnchor;      // Anchor point for the Monster to rotate around
    private Vector3 mOrbitDirection;    // Orbit direction
    private Vector3 mMonsterMaxScale;   // Max Monster Scale
    public float mGrowingSpeed = 10f;   // Growing Speed
    private bool mIsMonsterScaled = false;

    /////////////////////////////////////////
    //
    //          Rendering Vars
    //
    /////////////////////////////////////////

    private Renderer renderer;
    private Material[] normalColor;
    public Material[] collideColor;       // public allows this to be set in unity
	NavMeshAgent nav;
    Animator anim;                      // for use in animation

    /////////////////////////////////////////
    //
    //             Combat Vars
    //
    /////////////////////////////////////////

    public int mMonsterHealth = 100;        // Monster Health
    private bool mIsAlive = true;           // Define if the Monster is Alive
    public GameObject itemDroped;
    public int[] sigilDroppedIds;
    public int MoveSpeed = 3;
    private int MaxDist = 10;
    private int MinDist = 2;
	private GameObject Player;
	Transform playerPos;
    /////////////////////////////////////////
    //
    //             Methods
    //
    /////////////////////////////////////////

    void Start()
    {
        renderer = GetComponentInChildren<Renderer>();
        MonsterSettings();
		Player = GameObject.Find("Main Camera");
		nav = GetComponent <NavMeshAgent> ();
		anim = GetComponent <Animator> ();         // should get the animator attached to the gameobject
		playerPos = GameObject.FindGameObjectWithTag ("MainCamera").transform;
        anim.SetBool("walk", true);              // should set the walk animation to be true. might need to be changed becasue this might casue it to keep restaring the animation every drame

    }

    /// <summary>
    /// Initalizer for monster settings, defines most varables and inits most private variables
    /// </summary>
    private void MonsterSettings()
    {
        //If there is only one material to the monster, this should work fine
        normalColor = gameObject.GetComponentInChildren<SkinnedMeshRenderer>().materials;

        // defining the anchor point as the main camera
        mOrbitAnchor = Camera.main.transform;

        // defining the orbit direction
        float x = Random.Range(-1f, 1f);
        float y = Random.Range(-1f, 1f);
        float z = Random.Range(-1f, 1f);
        mOrbitDirection = new Vector3(x, y, z);

        // defining speed
        mOrbitSpeed = Random.Range(5f, mOrbitMaxSpeed);

        // defining scale
        float scale = Random.Range(mScaleMin, mScaleMax);
        mMonsterMaxScale = new Vector3(scale, scale, scale);

        // set Monster scale to 0, to grow it lates
        transform.localScale = Vector3.zero;
    }

    /// <summary>
    /// Rotates the monster around an anchor point, as well as rotating on it's own axis
    /// </summary>
    private void RotateMonster()
    {
        // rotate Monster around camera
        transform.RotateAround(
            mOrbitAnchor.position, mOrbitDirection, mOrbitSpeed * Time.deltaTime);

        // rotating around its axis
        transform.Rotate(mOrbitDirection * 30 * Time.deltaTime);
    }

    /// <summary>
    /// Moves the monster towards the player
    /// </summary>
    private void MoveMonster()
    {
        transform.LookAt(Camera.main.transform);
        if (Vector3.Distance(transform.position, Camera.main.transform.position) >= MinDist)
        {

            transform.position += transform.forward * MoveSpeed * Time.deltaTime;

            if (Vector3.Distance(transform.position, Camera.main.transform.position) <= MaxDist)
            {
                //Here Call any function you want Like Shoot at here or something
            }

        }
    }

    /// <summary>
    /// Update is called once per frame
    /// </summary>
    void Update()
    {
        if (mIsAlive == false)
        {
            Death();
        }
        else
        {
            // makes the Monster orbit and rotate
            //RotateMonster();

            // Makes the monster move towards the player (camera)
            //MoveMonster();
            nav.SetDestination (playerPos.position); // Move toward player using navmesh agent

            // scale Monster if needed
            if (!mIsMonsterScaled)
                ScaleObj();
        }

		// When the monster gets close enough to the player, attack
		if (Vector3.Distance(transform.position, Camera.main.transform.position) < MinDist) {
            anim.SetTrigger("attack");                          // should work, if it dosent might need to be an IEnumerator or has a wait thingy
			Player.GetComponent<PlayerBehaviorScript> ().HitPlayer (Random.Range(1, 10));
			if (Player.GetComponent<PlayerBehaviorScript>().mIsAlive == true){ //if the player is still alive...
				int s = 0; // Do some junk process to slow monster attacks.
				while (s < 9999){
					s++;
				}
			}
			// Destroy (gameObject); // Comment out for testing
		}

		// If the monster is clicked on, get hit.
		if (Input.GetMouseButtonDown(0)){
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			// take damage if hit
			if (Physics.Raycast(ray, out hit)){
				if (hit.collider.gameObject == this.gameObject) {
					Hit(25);
				}
			}
		}

		// Mobile controls for attacking monsters
		for (int i = 0; i < Input.touchCount; i++){
			if (Input.GetTouch(i).phase == TouchPhase.Began){
				Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(i).position);
				RaycastHit hit;
				// take damage if hit
				if (Physics.Raycast (ray, out hit)) {
					if (hit.collider.gameObject == this.gameObject) {
						Hit (25);
					}
				}
			}
		}

    }

    /// <summary>
    /// Scales the object from size 0% to 100%
    /// </summary>
    private void ScaleObj()
    {

        // growing obj
        if (transform.localScale != mMonsterMaxScale)
            transform.localScale = Vector3.Lerp(transform.localScale, mMonsterMaxScale, Time.deltaTime * mGrowingSpeed);
        else
            mIsMonsterScaled = true;
    }

    /// <summary>
    /// Handles damage being delt to the monster, sets object mIsAlive to false if the health is below zero, 
    /// which the Update function looks at and destorys the object if isAlive is false
    /// </summary>
    /// <param name="hitDamage"> quantity of damage dealt to the monster </param>
    public void Hit(int hitDamage)
    {
        Debug.Log("Hit activated");
        mMonsterHealth -= hitDamage;
        StartCoroutine(colourflash());
        //Debug.Log("flashcomplete");
        if (mMonsterHealth <= 0 && mIsAlive)
        {
            this.mIsAlive = false;
        }
    }

    /// <summary>
    /// Coroutine to briefly change the material of the monster to indicate damage being taken
    /// </summary>
    /// <returns></returns>
    IEnumerator colourflash()
    {
        renderer.materials = collideColor;
        yield return new WaitForSeconds(0.2f);
        renderer.materials =  normalColor;
    } 

    void Death()
    {
		nav.enabled = false; // Turn off the navmesh agent.
		//GameObject item  = Instantiate(itemsDrops[Random.Range(0, itemsDrops.Length)], transform.position, transform.rotation) as GameObject;
        GameObject item = Instantiate(itemDroped, transform.position, transform.rotation) as GameObject;
        item.GetComponent<SigilScript>().setSigilID(sigilDroppedIds[Random.Range(0, sigilDroppedIds.Length)]);
        // add in a bit which controlls what the item consiters its value as
		Player.GetComponent<PlayerBehaviorScript>().monsterDeaths += 1;
		if (Player.GetComponent<PlayerBehaviorScript>().monsterDeaths == Player.GetComponent<PlayerBehaviorScript>().monsterTarget){
			Player.GetComponent<PlayerBehaviorScript> ().monsterTarget += 5;
			GameObject.Find ("Monster_Spawner_Controller").GetComponent<SpawnScript> ().mMaxMonstersOnscreen += 1;
		}
        Destroy(this.gameObject);
    }
}
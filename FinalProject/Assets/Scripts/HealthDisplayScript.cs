﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplayScript : MonoBehaviour {
    public Slider healthBar;
    private Text textbox;

	// Use this for initialization
	void Start () {
        textbox = GetComponent<Text>();
        textbox.text = "0%";
	}
	
	// Update is called once per frame
	void Update () {
        textbox.text = ((healthBar.value / healthBar.maxValue) * 100).ToString() + "%";
    }
}

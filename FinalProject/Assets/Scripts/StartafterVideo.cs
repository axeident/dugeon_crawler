﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartafterVideo : MonoBehaviour {

	public VideoPlayer Tutorial;
	
	// Update is called once per frame
	void Update () {
		if (Tutorial.isPlaying == false){
			SceneManager.LoadScene ("Maze1");
		}
	}
}
